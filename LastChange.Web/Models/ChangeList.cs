﻿using System.Collections.Generic;

namespace LastChange.Web.Models
{
    public class ChangeList<T>
    {
        public long Version { get; set; }
        public List<T> List { get; set; }
    }
}