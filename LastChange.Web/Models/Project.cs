﻿using System;

namespace LastChange.Web.Models
{
    public class Project
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}