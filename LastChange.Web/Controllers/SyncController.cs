﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using Dapper;
using LastChange.Web.Models;

namespace LastChange.Web.Controllers
{
    public class SyncController : ApiController
    {
        public ChangeList<Project> GetChangedProjects(long id)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Win8Sync"].ConnectionString))
            {
                conn.Open();
                var result = new ChangeList<Project>
                {
                    Version = conn.Query<long>("SELECT CONVERT(bigint, MAX(Version)) FROM Project").First(),
                    List = conn.Query<Project>("SELECT Id, Name FROM Project WHERE Version > @Version", new { Version = id }).ToList()
                };
                return result;
            }
        }
    }
}