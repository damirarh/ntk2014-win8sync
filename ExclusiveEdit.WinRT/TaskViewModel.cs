﻿using System;
using SQLite;

namespace ExclusiveEdit.WinRT
{
    [Table("Task")]
    public class TaskViewModel
    {
        private bool _done;

        [PrimaryKey]
        public Guid Id { get; set; }
        public string Summary { get; set; }
        public string Priority { get; set; }
        public string Project { get; set; }

        public bool Done
        {
            get { return _done; }
            set
            {
                _done = value;
                MarkAsDone(Id, Done);
            }
        }

        public string Username { get; set; }

        private async void MarkAsDone(Guid id, bool done)
        {
            var conn = new SQLiteAsyncConnection("Win8Sync.db");
            await conn.ExecuteAsync("UPDATE Task SET Done = ? WHERE Id = ?", done, id);
        }

    }
}