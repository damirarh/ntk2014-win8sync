﻿using System;

namespace ExclusiveEdit.WinRT
{
    public class Lock
    {
        public Guid Id { get; set; }
        public string Summary { get; set; }
        public string LockedBy { get; set; }
        public bool ObtainLock { get; set; }
    }
}