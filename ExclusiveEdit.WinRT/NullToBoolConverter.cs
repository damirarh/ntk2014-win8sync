﻿using System;
using Windows.UI.Xaml.Data;

namespace ExclusiveEdit.WinRT
{
    public class NullToBoolConverter : IValueConverter
    {
        public bool NullValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return value == null ? NullValue : !NullValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}