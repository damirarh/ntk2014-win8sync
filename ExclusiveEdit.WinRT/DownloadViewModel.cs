﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.StoreApps;
using Newtonsoft.Json;
using SQLite;

namespace ExclusiveEdit.WinRT
{
    public class DownloadViewModel : BindableBase
    {
        private List<Lock> _locks;

        public List<Lock> Locks
        {
            get { return _locks; }
            set { SetProperty(ref _locks, value); }
        }

        public DownloadViewModel()
        {
            Init();
        }

        private async void Init()
        {
            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync("http://nb-damira/ExclusiveEdit.Web/api/lock/");
            Locks = JsonConvert.DeserializeObject<List<Lock>>(json);
        }

        public async Task OnDownload()
        {
            var conn = new SQLiteAsyncConnection("Win8Sync.db");
            foreach (var selectedLock in Locks.Where(l => l.ObtainLock))
            {
                var httpClient = new HttpClient();
                var content = new StringContent(JsonConvert.SerializeObject(new { selectedLock.Id, selectedLock.Summary, LockedBy = "damir"}), Encoding.UTF8, "application/json");
                var response = await httpClient.PutAsync("http://nb-damira/ExclusiveEdit.Web/api/lock/", content);

                if (response.StatusCode != HttpStatusCode.NoContent)
                {
                    throw new Exception("Error obtaining lock");
                }

                var json = await httpClient.GetStringAsync("http://nb-damira/ExclusiveEdit.Web/api/task/" + selectedLock.Id);
                var task = JsonConvert.DeserializeObject<TaskViewModel>(json);
                await conn.InsertAsync(task);
            }
        }
    }
}