﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.Prism.StoreApps;
using Newtonsoft.Json;
using SQLite;

namespace ExclusiveEdit.WinRT
{
    public class MainViewModel : BindableBase
    {
        private List<TaskViewModel> _tasks;

        public List<TaskViewModel> Tasks
        {
            get { return _tasks; }
            set { SetProperty(ref _tasks, value); }
        }

        public ICommand UploadCommand { get; set; }

        public MainViewModel()
        {
            Init();

            UploadCommand = new DelegateCommand(OnUpload);
        }

        private async void Init()
        {
            var conn = new SQLiteAsyncConnection("Win8Sync.db");
            await conn.CreateTableAsync<TaskViewModel>();
            await LoadTasks();
        }

        private async Task LoadTasks()
        {
            var conn = new SQLiteAsyncConnection("Win8Sync.db");
            Tasks = await conn.QueryAsync<TaskViewModel>("SELECT * FROM Task");
        }

        private async void OnUpload()
        {
            var conn = new SQLiteAsyncConnection("Win8Sync.db");
            foreach (var task in Tasks)
            {
                var httpClient = new HttpClient();
                var content = new StringContent(JsonConvert.SerializeObject(task), Encoding.UTF8, "application/json");
                await httpClient.PutAsync("http://nb-damira/ExclusiveEdit.Web/api/task/", content);

                await conn.DeleteAsync(task);
            }
            await LoadTasks();
        }
    }
}