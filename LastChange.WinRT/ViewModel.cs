﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Data.Json;
using Microsoft.Practices.Prism.StoreApps;
using Newtonsoft.Json;
using SQLite;

namespace LastChange.WinRT
{
    public class ViewModel : BindableBase
    {
        private List<Project> _projects;
        public ICommand SyncCommand { get; set; }

        public List<Project> Projects
        {
            get { return _projects; }
            set { SetProperty(ref _projects, value); }
        }

        public ViewModel()
        {
            SyncCommand = new DelegateCommand(OnSync);
            Init();
        }

        private async void OnSync()
        {
            var conn = new SQLiteAsyncConnection("Win8Sync.db");
            var syncVersion = (await conn.QueryAsync<SyncVersion>("SELECT * FROM SyncVersion WHERE Entity = ?", typeof (Project).Name)).FirstOrDefault();
            var version = syncVersion != null ? syncVersion.Version : 0;

            var httpClient = new HttpClient();
            var json = await httpClient.GetStringAsync("http://nb-damira/LastChange.Web/api/sync/" + version);
            var changeList = JsonConvert.DeserializeObject<ChangeList<Project>>(json);

            foreach (var project in changeList.List)
            {
                await UpdateOrInsert(conn, project);
            }
            await UpdateOrInsert(conn, new SyncVersion(typeof (Project), changeList.Version));

            await LoadProjects();
        }

        private static async Task UpdateOrInsert(SQLiteAsyncConnection conn, object item)
        {
            var updateCount = await conn.UpdateAsync(item);
            if (updateCount == 0)
            {
                await conn.InsertAsync(item);
            }
        }

        private async void Init()
        {
            var conn = new SQLiteAsyncConnection("Win8Sync.db");
            await conn.CreateTableAsync<SyncVersion>();
            await conn.CreateTableAsync<Project>();
            await LoadProjects();
        }

        private async Task LoadProjects()
        {
            var conn = new SQLiteAsyncConnection("Win8Sync.db");
            Projects = await conn.QueryAsync<Project>("SELECT * FROM Project");
        }
    }
}