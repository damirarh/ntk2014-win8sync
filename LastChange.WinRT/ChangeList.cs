﻿using System.Collections.Generic;

namespace LastChange.WinRT
{
    public class ChangeList<T>
    {
        public List<T> List { get; set; }
        public long Version { get; set; }
    }
}