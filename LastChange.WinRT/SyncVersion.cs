﻿using System;
using SQLite;

namespace LastChange.WinRT
{
    public class SyncVersion
    {
        [PrimaryKey]
        public string Entity { get; set; }

        public long Version { get; set; }

        public SyncVersion()
        {
        }

        public SyncVersion(Type entityType, long version)
        {
            Entity = entityType.Name;
            Version = version;
        }
    }
}