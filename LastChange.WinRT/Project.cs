﻿using System;
using SQLite;

namespace LastChange.WinRT
{
    public class Project
    {
        [PrimaryKey]
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}