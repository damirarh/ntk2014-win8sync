﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Microsoft.Practices.Prism.StoreApps;
using SQLite;
using Win8Sync;

namespace SyncFrameworkToolkit.WinRT
{
    public class MainViewModel : BindableBase
    {
        private string _message;
        private List<TaskViewModel> _tasks;

        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }

        public ICommand SyncCommand { get; set; }

        public List<TaskViewModel> Tasks
        {
            get { return _tasks; }
            set { SetProperty(ref _tasks, value); }
        }

        public MainViewModel()
        {
            SyncCommand = new DelegateCommand(OnSync);

            LoadTasks();
        }

        private async void LoadTasks()
        {
            try
            {
                var conn = new SQLiteAsyncConnection("Win8Sync.db");
                Tasks = await conn.QueryAsync<TaskViewModel>("SELECT T.Id, T.Summary, P.Label AS Priority, Proj.Name AS Project, U.FullName AS User, T.Done FROM Task T INNER JOIN Priority P ON p.Value = T.PriorityValue INNER JOIN Project Proj ON Proj.Id = T.ProjectId LEFT OUTER JOIN User U ON U.Username = T.Username");
            }
            catch { } // ignore missing table before synchronization
        }

        private async void OnSync()
        {
            try
            {
                Message = "Synchronization in Progress...";

                var serviceUri = new Uri("http://nb-damira/SyncFrameworkToolkit.Web/Win8SyncSyncService.svc");
                var ctx = new Win8SyncOfflineContext("Win8Sync.db", serviceUri);

                var stats = await ctx.SynchronizeAsync();

                if (stats.Error != null)
                {
                    Message = stats.Error.Message;
                    return;
                }

                Message = stats.TotalDownloads.ToString();
                LoadTasks();
            }
            catch (Exception e)
            {
                Message = e.Message;
            }
        }
    }
}