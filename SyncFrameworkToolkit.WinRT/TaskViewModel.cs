﻿using System;
using SQLite;

namespace SyncFrameworkToolkit.WinRT
{
    public class TaskViewModel
    {
        private bool _done;
        public Guid Id { get; set; }
        public string Summary { get; set; }
        public string Priority { get; set; }
        public string Project { get; set; }
        public string User { get; set; }

        public bool Done
        {
            get { return _done; }
            set
            {
                _done = value;
                MarkAsDone(Id, Done);
            }
        }

        private async void MarkAsDone(Guid id, bool done)
        {
            var conn = new SQLiteAsyncConnection("Win8Sync.db");
            await conn.ExecuteAsync("UPDATE Task SET Done = ? WHERE Id = ?", done, id);
        }
    }
}