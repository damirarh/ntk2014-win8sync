﻿using System;
using System.Web.Services.Configuration;

namespace ExclusiveEdit.Web.Models
{
    public class Task
    {
        public Guid Id { get; set; }
        public string Summary { get; set; }
        public string Priority { get; set; }
        public string Project { get; set; }
        public string Username { get; set; }
        public bool Done { get; set; }
    }
}