﻿using System;

namespace ExclusiveEdit.Web.Models
{
    public class Lock
    {
        public Guid Id { get; set; }
        public string Summary { get; set; }
        public string LockedBy { get; set; }
    }
}