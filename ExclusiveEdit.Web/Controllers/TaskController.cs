﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using Dapper;
using ExclusiveEdit.Web.Models;

namespace ExclusiveEdit.Web.Controllers
{
    public class TaskController : ApiController
    {
        public Task GetTask(Guid id)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Win8Sync"].ConnectionString))
            {
                return conn.Query<Task>("SELECT T.Id, T.Summary, P.Label AS Priority, Proj.Name AS Project, T.Username, T.Done FROM Task T INNER JOIN Priority P ON p.Value = T.PriorityValue INNER JOIN Project Proj ON Proj.Id = T.ProjectId WHERE T.ID = @Id", new { Id = id }).FirstOrDefault();
            }
        }

        public void PutTask(Task task)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Win8Sync"].ConnectionString))
            {
                if (conn.Execute("UPDATE Task SET Done = @Done, Username = NULL WHERE Id = @Id AND Username = @Username", new { task.Done, task.Id, task.Username }) == 0)
                {
                    throw new Exception("Failed to update task.");
                }
            }
        }
    }
}