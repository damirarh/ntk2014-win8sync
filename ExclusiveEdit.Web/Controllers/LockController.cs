﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Http;
using Dapper;
using ExclusiveEdit.Web.Models;

namespace ExclusiveEdit.Web.Controllers
{
    public class LockController : ApiController
    {
        public IEnumerable<Lock> GetAllLocks()
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Win8Sync"].ConnectionString))
            {
                return conn.Query<Lock>("SELECT Id, Summary, Username AS LockedBy FROM Task");
            }
        }

        public void PutLock(Lock item)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Win8Sync"].ConnectionString))
            {
                if (conn.Execute("UPDATE Task SET Username = @Username WHERE Id = @Id AND Username IS NULL", new { item.Id, Username = item.LockedBy }) == 0)
                {
                    throw new Exception("Failed to obtain lock");
                }
            }
        }
    }
}