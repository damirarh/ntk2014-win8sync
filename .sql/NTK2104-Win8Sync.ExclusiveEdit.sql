/****** Object:  Table [dbo].[Priority]    Script Date: 4.10.2013 21:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Priority](
	[Value] [int] NOT NULL,
	[Label] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Priority] PRIMARY KEY CLUSTERED 
(
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Project]    Script Date: 4.10.2013 21:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Project](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Project] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Task]    Script Date: 4.10.2013 21:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Task](
	[Id] [uniqueidentifier] NOT NULL,
	[Summary] [nvarchar](100) NULL,
	[Description] [nvarchar](1024) NULL,
	[PriorityValue] [int] NOT NULL,
	[ProjectId] [uniqueidentifier] NOT NULL,
	[Username] [nvarchar](20) NULL,
	[Done] [bit] NOT NULL,
 CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 4.10.2013 21:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Username] [nvarchar](20) NOT NULL,
	[FullName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Priority] ([Value], [Label]) VALUES (1, N'Low')
INSERT [dbo].[Priority] ([Value], [Label]) VALUES (2, N'Normal')
INSERT [dbo].[Priority] ([Value], [Label]) VALUES (3, N'High')
INSERT [dbo].[Project] ([Id], [Name]) VALUES (N'24fa36af-03e6-454c-b7c2-4d20278c6859', N'NTK')
INSERT [dbo].[Project] ([Id], [Name]) VALUES (N'd6f4f323-309f-4434-aa91-efa2afeb9646', N'Blog')
INSERT [dbo].[Task] ([Id], [Summary], [Description], [PriorityValue], [ProjectId], [Username], [Done]) VALUES (N'57d4cbef-b2da-41c8-8935-8b0901f8ee55', N'Sinhronizacija podatkov - praktični vodič za izgradnjo pametnih Windows 8 aplikacij', NULL, 2, N'24fa36af-03e6-454c-b7c2-4d20278c6859', NULL, 0)
INSERT [dbo].[Task] ([Id], [Summary], [Description], [PriorityValue], [ProjectId], [Username], [Done]) VALUES (N'0ed414c4-45c8-46da-82a8-8d504b321e70', N'Novosti v razvoju za Windows 8.1 s primeri uporabe', NULL, 2, N'24fa36af-03e6-454c-b7c2-4d20278c6859', NULL, 0)
INSERT [dbo].[Task] ([Id], [Summary], [Description], [PriorityValue], [ProjectId], [Username], [Done]) VALUES (N'6a95ae4e-521b-4281-9a54-bbea0b021153', N'Razvoj večpredstavnostnih aplikacij v Windows 8', NULL, 2, N'24fa36af-03e6-454c-b7c2-4d20278c6859', NULL, 0)
INSERT [dbo].[Task] ([Id], [Summary], [Description], [PriorityValue], [ProjectId], [Username], [Done]) VALUES (N'92bae1c4-ab2d-46d0-97c2-e6b163ba5430', N'Migracija Windows 8 na Windows 8.1', NULL, 2, N'24fa36af-03e6-454c-b7c2-4d20278c6859', NULL, 0)
INSERT [dbo].[User] ([Username], [FullName]) VALUES (N'andrej', N'Andrej Tozon')
INSERT [dbo].[User] ([Username], [FullName]) VALUES (N'damir', N'Damir Arh')
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_Priority] FOREIGN KEY([PriorityValue])
REFERENCES [dbo].[Priority] ([Value])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_Priority]
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_Project] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Project] ([Id])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_Project]
GO
ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_User] FOREIGN KEY([Username])
REFERENCES [dbo].[User] ([Username])
GO
ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_User]
GO
